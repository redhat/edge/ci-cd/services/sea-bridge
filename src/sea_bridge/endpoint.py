#!/usr/bin/env python3

import hashlib
import threading

from flask import Flask, jsonify, request, abort

from sea_bridge.sync import trigger_sync

STATE_WAITING = "waiting"
STATE_SYNCING = "syncing"


def is_valid_api_key(api_key: str, valid_api_keys: list[dict[str:dict]]) -> bool:
    for valid_api_key in valid_api_keys:
        salt = valid_api_key["salt"]
        expected_digest = valid_api_key["sha512_digest"]
        actual_digest = hashlib.sha512((salt + api_key).encode("utf-8")).hexdigest()

        if actual_digest == expected_digest:
            return True

    return False


def create_app(config, scheduler, sync_lock: threading.Lock):
    """Creates a flask app to serve the RESTful endpoint for sea-bridge"""
    app = Flask(__name__)

    # See http://flask.pocoo.org/docs/latest/config/
    app.config.update({"DEBUG": True})
    app.config.update(config or {})

    @app.errorhandler(403)
    def unauthorized(e):
        return jsonify(error=str(e)), 403

    @app.route("/")
    @app.route("/readyz")
    @app.route("/livez")
    def index():
        s3_debug_config_keys = [
            "s3_bucket",
            "s3_prefixes",
        ]
        artifactory_debug_config_keys = [
            "artifactory_url",
            "artifactory_repo",
            "artifactory_upload_prefix",
        ]
        liveliness = {
            "status": "ready",
        }
        for key in s3_debug_config_keys:
            liveliness[key] = app.config["aws_s3"][key]

        for key in artifactory_debug_config_keys:
            liveliness[key] = app.config["artifactory"][key]

        if "interval" in app.config:
            liveliness["interval"] = app.config["interval"]

        return jsonify(liveliness)

    @app.route("/sync", methods=["GET", "POST"])
    def sync():
        if request.method == "POST":
            # check api_key matches app configuration
            if "api_key" not in request.form:
                abort(403, description="no api_key provided")

            if not is_valid_api_key(
                request.form["api_key"], app.config.get("api_keys")
            ):
                abort(403, description="invalid api_key")

            # add an immediate job to the scheduler if POSTed
            trigger_sync(scheduler, config, sync_lock)

        # Convert scheduler list of jobs to nice dict for response
        response_jobs = []
        scheduler_jobs = scheduler.get_jobs()
        for job in scheduler_jobs:
            response_jobs.append(
                {
                    "job_id": f"{job.id}",
                    "next_run_time": f"{job.next_run_time}",
                    "trigger": f"{job.trigger}",
                }
            )

        # Sync job will lock sync_lock while it's syncing
        response_state = STATE_SYNCING if sync_lock.locked() else STATE_WAITING

        response = {
            "possible_states": [STATE_WAITING, STATE_SYNCING],
            "current_state": response_state,
            "pending_sync_jobs": response_jobs,
        }
        return jsonify(response)

    return app
