from numbers import Number
from typing import Mapping, List, Optional, Union, Collection

JsonObject = Mapping[str, "JsonValue"]
JsonArray = List["JsonValue"]
JsonValue = Optional[
    Union[
        JsonObject,
        JsonArray,
        str,
        Number,
        bool,
    ]
]
Config = Collection[JsonObject]
