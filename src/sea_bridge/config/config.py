import os

from pathlib import Path
from typing import Any, Mapping, Collection

import yaml
from jinja2 import Environment
from jsonschema import validate

from sea_bridge.custom_types import Config

HERE = Path(__file__).parent
DEFAULT_CONFIG_PATH = HERE / "config.yml"
DEFAULT_SCHEMA_PATH = HERE / "config.schema.yml"


def parse_and_validate(
    environment: Environment = Environment(),
    path: Path = DEFAULT_CONFIG_PATH,
    schema_path: Path = DEFAULT_SCHEMA_PATH,
    **kwargs
) -> Collection[Mapping[str, Any]]:
    """Parses the config at ``path`` and validates it against the schema at ``schema_path``.

    This function is convenience for calling both :func:`parse_config`
    and :func`validate_config`.

    Parameters:
        environment: A :class:`jinja.Environment` that will be used to build a Template.
        path: The path to the configuration file to be parsed. Default: config.yml
        schema_path: The path to the schema file to validate the config against.
            Default: config.schema.yml
        kwargs: Any keyword arguments to be passed to Jinja when rendering the
            configuration file.

    See Also:
        :func:`parse_config`
        :func:`validate_config`
    """
    # Make environment variables available to config parser
    environment.globals["env"] = os.environ

    config = parse_config(environment, path, **kwargs)
    validate_config(config, schema_path=schema_path)
    return config


def parse_config(environment: Environment, path: Path, **kwargs) -> Config:
    """Parse the configuration file at ``path``.

    This function first passes the content from ``path`` to Jinja for
    rendering before parsing from YAML. Any keyword arguments that should be
    passed to Jinja for rendering should be in ``kwargs``.

    Parameters:
        environment: A :class:`jinja.Environment` that will be used to build a Template.
        path: The path to the configuration file to be parsed. Default: config.yml
        kwargs: Any keyword arguments to be passed to Jinja when rendering the
            configuration file.
    """
    with path.open("r") as in_:
        template = environment.from_string(in_.read())
    rendered = template.render(**kwargs)
    return yaml.safe_load(rendered)


def validate_config(config: Config, schema_path: Path):
    """Validates ``config`` against the schema at ``schema_path``.

    Parameters:
        config: An instance of the configuration data.
        schema_path: The path to the schema file to validate the config against.
            Default: triggers.schema.yml

    Raises:
        :class:jsonschema.ValidationError: if the provided instance does not match
            the schema.
    """
    with schema_path.open("r") as in_:
        schema = yaml.safe_load(in_)

    validate(config, schema)
