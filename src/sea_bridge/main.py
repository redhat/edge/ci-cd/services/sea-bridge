#!/usr/bin/env python3

import argparse
import pathlib
import sys
import threading

from jinja2 import Environment

from sea_bridge.endpoint import create_app
from sea_bridge.sync import get_scheduler, schedule_sync
from .config import parse_and_validate


def argparser() -> argparse.ArgumentParser:
    """Argparser configuration for sea-bridge"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--config",
        type=pathlib.Path,
        required=True,
        help="Path to the config.yaml file",
    )
    parser.add_argument("--port", type=int, default=8080, help="REST API endpoint port")

    return parser


def main() -> None:
    """Entrypoint for sea-bridge"""
    parser = argparser()
    args = parser.parse_args()

    # Check configuration against schema
    # We want to verify all mandatory options are set otherwise throw an error
    config = parse_and_validate(path=args.config, environment=Environment())
    if not config:
        print(f"Error: could not load configuration file: {args.config}")
        sys.exit(1)

    # Create a shared lock to ensure only one sync job is active at a time
    # And to also be able to trivially share state with the flask app
    sync_lock = threading.Lock()

    # Get background scheduler and put it in config to make it global
    scheduler = get_scheduler()

    # If interval configuration option set, schedule the job
    if config.get("interval"):
        print(f"Scheduling sync every {config['interval']} minutes")
        schedule_sync(scheduler, config, sync_lock)

    # Spawn web api
    app = create_app(config, scheduler, sync_lock)
    app.run(host="0.0.0.0", port=args.port)


if __name__ == "__main__":
    main()
