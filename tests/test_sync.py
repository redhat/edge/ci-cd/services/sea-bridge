#!/usr/bin/env python3

import argparse
import pytest
import threading

from unittest import TestCase
from unittest.mock import patch

import artifactory
import boto3

from apscheduler.schedulers.base import STATE_RUNNING
from apscheduler.schedulers.background import BackgroundScheduler

import sea_bridge.sync


def test_get_scheduler():
    """Test get_scheduler()

    Check that calling get_scheduler returns an BackgroundScheduler
    with the expected attributes and state needed for sea-bridge.
    """
    scheduler = sea_bridge.sync.get_scheduler()
    assert type(scheduler) is BackgroundScheduler

    assert "coalesce" in scheduler._job_defaults
    assert scheduler._job_defaults["coalesce"] == True
    assert "max_instances" in scheduler._job_defaults
    assert scheduler._job_defaults["max_instances"] == 1

    assert scheduler.state == STATE_RUNNING


def test_schedule_sync(fake_scheduler, fake_config, fake_sync_lock):
    """Test schedule_sync()

    Check that calling schedule_sync results in a new job added to the
    scheduler with the expected attributes for an interval job.
    """
    fake_config["interval"] = 100
    sea_bridge.sync.schedule_sync(fake_scheduler, fake_config, fake_sync_lock)

    assert len(fake_scheduler.jobs) == 1
    assert fake_scheduler.jobs[0]["func"] == sea_bridge.sync._sync_s3_to_artifactory
    assert fake_scheduler.jobs[0]["args"] == [fake_config, fake_sync_lock]
    assert fake_scheduler.jobs[0]["trigger"] == "interval"
    assert fake_scheduler.jobs[0]["trigger_args"] == {"minutes": 100}


def test_trigger_sync(fake_scheduler, fake_config, fake_sync_lock):
    """Test trigger_sync()

    Check that calling schedule_sync results in a new job added to the
    scheduler with the expected attributes for an immediate job.
    """
    sea_bridge.sync.trigger_sync(fake_scheduler, fake_config, fake_sync_lock)

    assert len(fake_scheduler.jobs) == 1
    assert fake_scheduler.jobs[0]["func"] == sea_bridge.sync._sync_s3_to_artifactory
    assert fake_scheduler.jobs[0]["args"] == [fake_config, fake_sync_lock]
    assert fake_scheduler.jobs[0]["trigger"] == "date"
    assert fake_scheduler.jobs[0]["trigger_args"] == {}


@patch("sea_bridge.sync._connect_to_sync_resources")
@patch("sea_bridge.sync._get_s3_objects_to_sync")
@patch("sea_bridge.sync._sync_s3_objects_to_artifactory_repo")
def test_sync_s3_to_artifactory(
    mock_sync_s3_objects_to_artifactory_repo,
    mock_get_s3_objects_to_sync,
    mock_connect_to_sync_resources,
    fake_config,
    fake_sync_lock,
    fake_s3_bucket,
    fake_artifactory_path,
):
    """Test _sync_s3_to_artifactory()

    Check that calling _sync_s3_to_artifactory calls the expected subroutines
    to do a synchronisation of s3 to artifactory.
    """
    fake_s3_object_summaries = []

    mock_connect_to_sync_resources.return_value = (
        fake_s3_bucket,
        fake_artifactory_path,
    )
    mock_get_s3_objects_to_sync.return_value = fake_s3_object_summaries
    sea_bridge.sync._sync_s3_to_artifactory(fake_config, fake_sync_lock)

    assert fake_sync_lock.count_locked == 1
    assert fake_sync_lock.count_unlocked == 1

    mock_connect_to_sync_resources.assert_called_once()
    mock_get_s3_objects_to_sync.assert_called_once()
    mock_sync_s3_objects_to_artifactory_repo.assert_called_once()


@patch("sea_bridge.sync._connect_to_sync_resources")
@patch("sea_bridge.sync._get_s3_objects_to_sync")
@patch("sea_bridge.sync._sync_s3_objects_to_artifactory_repo")
def test_sync_s3_to_artifactory_exception_handling(
    mock_sync_s3_objects_to_artifactory_repo,
    mock_get_s3_objects_to_sync,
    mock_connect_to_sync_resources,
    fake_config,
    fake_sync_lock,
    fake_s3_bucket,
    fake_artifactory_path,
):
    """Test if _sync_s3_to_artifactory() handles exceptions gracefully

    Extends checks on _sync_s3_to_artifactory to ensure it handles the
    exceptions gracefully by not continuing to synchronise and to release
    the sync lock as needed to unblock the next synchronisation job.
    """
    mock_connect_to_sync_resources.return_value = (
        fake_s3_bucket,
        fake_artifactory_path,
    )
    mock_get_s3_objects_to_sync.side_effect = Exception("an error")
    sea_bridge.sync._sync_s3_to_artifactory(fake_config, fake_sync_lock)

    assert fake_sync_lock.count_locked == 1
    assert fake_sync_lock.count_unlocked == 1

    mock_connect_to_sync_resources.assert_called_once()
    mock_get_s3_objects_to_sync.assert_called_once()
    mock_sync_s3_objects_to_artifactory_repo.assert_not_called()


@patch("artifactory.ArtifactoryPath.touch")
def test_connect_to_sync_resources(
    mock_artifactorypath_touch, fake_s3_bucket, fake_config
):
    """Test _connect_to_sync_resources()

    Check that calling _connect_to_sync_resources will create the needed
    s3 bucket and artifactory path resources. The touch method is mocked
    as the ArtifactoryPath points to a non-existent resource so it would
    fail with network timeouts if attempted to be actually manipulated.

    The s3_bucket returned is a useable mock. The fake_config points the
    function to access the fake_s3_bucket mock.
    """
    s3_bucket, artifactory_path = sea_bridge.sync._connect_to_sync_resources(
        fake_config
    )

    assert s3_bucket is not None
    assert artifactory_path is not None

    mock_artifactorypath_touch.assert_called_once()


def test_get_s3_objects_to_sync(fake_config, fake_s3_bucket):
    """Test _get_s3_objects_to_sync()

    Checks that calling _get_s3_objects_to_sync will return a filtered down
    set of s3 object summaries based on the contents of an s3 bucket and the
    configured s3_prefixes wanted. The s3 prefixes for this test are defined
    in the fake_config mock. The s3 bucket contents are defined in the
    fake_s3_bucket mock. The expected object summaries is hardcoded based on
    these mocks.
    """
    expected_object_summaries = ["a", "a/b", "a/b/c", "d/c"]
    actual_object_summaries = sea_bridge.sync._get_s3_objects_to_sync(
        fake_s3_bucket, fake_config["aws_s3"]["s3_prefixes"]
    )

    assert actual_object_summaries == expected_object_summaries


def test_sync_s3_objects_to_artifactory_repo(
    fake_s3_bucket, fake_artifactory_path, capsys
):
    """Test _sync_s3_objects_to_artifactory_repo()

    Check that calling _sync_s3_objects_to_artifactory_repo will call the
    _upload_artifactory function as many times as expected given a certain set
    of s3 objects. Also checks the stdout matches expectations.

    The _clean_artifactory_path call inside is mocked out to make it innert
    and that functionality is tested in a different test.
    """
    s3_objects = ["a", "d/c", "a/b/"]
    with patch("sea_bridge.sync._artifactory_upload") as mock_artifactory_upload, patch(
        "sea_bridge.sync._clean_artifactory_path"
    ) as mock_clean_artifactory_path:
        sea_bridge.sync._sync_s3_objects_to_artifactory_repo(
            fake_s3_bucket, fake_artifactory_path, s3_objects
        )

        assert mock_artifactory_upload.call_count == 2
        mock_clean_artifactory_path.assert_called_once

        out, err = capsys.readouterr()
        assert "Syncing: a" in out
        assert "Syncing: d/c" in out
        assert "Syncing: a/b/" in out
        assert "Syncing: b/c" not in out


@patch("artifactory.ArtifactoryPath.unlink")
def test_clean_artifactory_path(mock_unlink, fake_artifactory_path, capsys):
    """Test _clean_artifactory_path()

    Checks that calling _clean_artifactory_path will call ArtifactoryPath.unlink
    the expected number of times. Also checks the stdout matches expectations.
    """
    s3_objects = ["a", "d/c", "a/b/"]
    fresh_fake_artifactory_paths = [
        fake_artifactory_path / "a",
        fake_artifactory_path / "d/c",
        fake_artifactory_path / "a/b",
    ]
    stale_fake_artifactory_paths = [
        fake_artifactory_path / "a/b/deleteme",
        fake_artifactory_path / "deleteme",
    ]

    with patch("sea_bridge.sync._walk_artifactory_path") as mock_walk_artifactory_path:
        mock_walk_artifactory_path.return_value = (
            fresh_fake_artifactory_paths + stale_fake_artifactory_paths
        )
        sea_bridge.sync._clean_artifactory_path(fake_artifactory_path, s3_objects)

        mock_unlink.assert_called()
        assert mock_unlink.call_count == 2

    out, err = capsys.readouterr()

    for path in stale_fake_artifactory_paths:
        assert f"Deleting: {path}" in out


@patch("artifactory.walk")
def test_walk_artifactory_path(mock_artifactory_walk, fake_artifactory_path):
    """Test _walk_artifactory_path()

    Checks that _walk_artifactory_path will call artifactory.walk and produce a
    generator based on those results that yields the total file and directory
    structure as expected.
    """
    mock_artifactory_walk.return_value = [
        (fake_artifactory_path, ["dir_a", "dir_d"], ["file_a", "file_b", "file_c"]),
        (fake_artifactory_path / "dir_a", ["dir_f"], ["file_g"]),
    ]
    # order matters
    expected_paths = [
        fake_artifactory_path / "file_a",
        fake_artifactory_path / "file_b",
        fake_artifactory_path / "file_c",
        fake_artifactory_path / "dir_a",
        fake_artifactory_path / "dir_d",
        fake_artifactory_path / "dir_a" / "file_g",
        fake_artifactory_path / "dir_a" / "dir_f",
    ]
    actual_paths = list(sea_bridge.sync._walk_artifactory_path(fake_artifactory_path))

    assert actual_paths == expected_paths


def test_s3_prefixes():
    """Test _s3_prefixes()

    Checks that _s3_prefixes will extract the expected directory prefixes from
    a set of paths.
    """
    s3_objects = ["a/b/c", "a/b/d", "b/c/d", "b/d/c", "c"]
    expected_prefixes = set(["a/b", "a", "b/c", "b", "b/d"])
    actual_prefixes = sea_bridge.sync._s3_prefixes(s3_objects)
    assert expected_prefixes == actual_prefixes


@patch("artifactory.ArtifactoryPath.exists")
@patch("artifactory.ArtifactoryPath.stat")
@patch("artifactory.ArtifactoryPath.deploy_by_checksum")
@patch("artifactory.ArtifactoryPath.deploy_file")
def test_artifactory_upload_already_exists(
    mock_deploy_file,
    mock_deploy_by_checksum,
    mock_stat,
    mock_exists,
    fake_artifactory_path,
    fake_local_file,
    fake_local_file_stat,
):
    """Test _artifactory_upload() when the upload files already exist

    Checks that _artifactory_upload will not call
    ArtifactoryPath.deploy_by_checksum and ArtifactoryPath.deploy_file when the
    file does already exist (with the correct checksum) on the Artifactory
    upload path.
    """
    mock_exists.return_value = True
    mock_stat.return_value = fake_local_file_stat
    sea_bridge.sync._artifactory_upload(fake_local_file, fake_artifactory_path)
    mock_deploy_by_checksum.assert_not_called()
    mock_deploy_file.assert_not_called()


@patch("artifactory.ArtifactoryPath.exists")
@patch("artifactory.ArtifactoryPath.deploy_by_checksum")
@patch("artifactory.ArtifactoryPath.deploy_file")
def test_artifactory_upload_by_checksum(
    mock_deploy_file,
    mock_deploy_by_checksum,
    mock_exists,
    fake_artifactory_path,
    fake_local_file,
    capsys,
):
    """Test _artifactory_upload() when the upload by checksum is possible

    Checks that _artifactory_upload will call ArtifactoryPath.deploy_by_checksum
    and not call ArtifactoryPath.deploy_file when the file does not already
    exist (with the correct checksum) on the Artifactory upload path and when it
    is possible to optimise the upload by using the upload-by-checksum feature.
    """
    mock_exists.return_value = False

    sea_bridge.sync._artifactory_upload(fake_local_file, fake_artifactory_path)
    mock_deploy_by_checksum.assert_called_once()
    mock_deploy_file.assert_not_called()

    out, err = capsys.readouterr()
    assert f"Uploaded-by-checksum: " in out
    assert f"Uploaded: " not in out


@patch("artifactory.ArtifactoryPath.exists")
@patch("artifactory.ArtifactoryPath.deploy_by_checksum")
@patch("artifactory.ArtifactoryPath.deploy_file")
def test_artifactory_upload_by_upload(
    mock_deploy_file,
    mock_deploy_by_checksum,
    mock_exists,
    fake_artifactory_path,
    fake_local_file,
    capsys,
):
    """Test _artifactory_upload() when the actual upload must be done

    Checks that _artifactory_upload will call ArtifactoryPath.deploy_file
    when the file does not already exist (with the correct checksum) on the
    Artifactory upload path and when it's not possible to optimise the upload
    by using the upload-by-checksum feature.
    """
    mock_exists.return_value = False
    mock_deploy_by_checksum.side_effect = Exception("an error")

    sea_bridge.sync._artifactory_upload(fake_local_file, fake_artifactory_path)
    mock_deploy_by_checksum.assert_called_once()
    mock_deploy_file.assert_called_once()

    out, err = capsys.readouterr()
    assert f"Uploaded-by-checksum: " not in out
    assert f"Uploaded: " in out
