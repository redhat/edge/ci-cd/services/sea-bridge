#!/usr/bin/env python3

"""Test Configurations"""

import os
import pytest

import artifactory
import boto3

from apscheduler.util import (
    asbool,
    asint,
    astimezone,
    maybe_ref,
    timedelta_seconds,
    undefined,
    TIMEOUT_MAX,
)
from moto import mock_s3


class FakeStat:
    sha256 = "14f8f4bb8c0e79a02670a5fea5682da717a5b3d3dc7b1706f7a4bab9afae18c2"


class FakeScheduler:
    """A fake scheduler for testing"""

    def __init__(self, gconfig={}, **options):
        self.jobs = []

    def get_jobs(self):
        return []

    def add_job(
        self,
        func,
        trigger=None,
        args=None,
        kwargs=None,
        id=None,
        name=None,
        misfire_grace_time=undefined,
        coalesce=undefined,
        max_instances=undefined,
        next_run_time=undefined,
        jobstore="default",
        executor="default",
        replace_existing=False,
        **trigger_args
    ):
        new_job = {
            "func": func,
            "trigger": trigger,
            "args": args,
            "kwargs": kwargs,
            "id": id,
            "name": name,
            "misfire_grace_time": misfire_grace_time,
            "coalesce": coalesce,
            "max_instances": max_instances,
            "next_run_time": next_run_time,
            "jobstore": jobstore,
            "executor": executor,
            "replace_existing": replace_existing,
            "trigger_args": trigger_args,
        }
        self.jobs.append(new_job)


class FakeThreadingLock:
    """A fake Threading.lock mock for testing"""

    def __init__(self):
        self.count_locked = 0
        self.count_unlocked = 0
        self.am_locked = False

    def acquire(self):
        self.count_locked += 1
        self.am_locked = True

    def release(self):
        self.count_unlocked += 1
        self.am_locked = False

    def locked(self):
        return self.am_locked


@pytest.fixture
def fake_s3_bucket():
    moto = mock_s3()
    moto.start()

    s3 = boto3.resource("s3", region_name="us-east-1")
    s3_bucket = s3.create_bucket(Bucket="my-bucket")
    s3.Bucket("my-bucket").put_object(Key="a", Body="a")
    s3.Bucket("my-bucket").put_object(Key="a/b", Body="ab")
    s3.Bucket("my-bucket").put_object(Key="a/b/c", Body="abc")
    s3.Bucket("my-bucket").put_object(Key="d/c", Body="dc")

    yield s3_bucket

    moto.stop()


@pytest.fixture
def fake_api_keys():
    api_keys = [
        {
            "api_key": "jOZxP$HUcgj&XK~vQW8u",
            "sha512_digest": "ee4ee34902f29e84e555a3639bf2bc7a9a2a9319b0d1d81904a0df30cea89a49f4862c9d34a95e9663632c2059647415e3cf05cc2f3a257de79f34a3c8a3016e",
            "salt": "PFxKFr9Cvc>W&$@3Pl58",
        },
        {
            "api_key": "u43CSixraK<D8@hPwXg_",
            "sha512_digest": "3f4b857785558e1765bab3d703d18094908fb7d680b8e6b4ee2f439b4ff13c9c788fc116a86196d26f48e31b2e816638d13e7ab44aac51fffa3d24e2ed61d4cb",
            "salt": "Fsi0M_o2b?$8@&gHOLnp",
        },
    ]

    yield api_keys


@pytest.fixture
def fake_config(fake_api_keys):
    fake_config = {
        "aws_s3": {
            "s3_prefixes": ["a", "b/c", "d"],
            "s3_credentials": {"access_key_id": "XXX", "secret_access_key": "YYY"},
            "s3_bucket": "my-bucket",
        },
        "artifactory": {
            "artifactory_url": "http://artifactory.local:8080",
            "artifactory_repo": "my-repo",
            "artifactory_upload_prefix": "my-upload-prefix",
            "artifactory_credentials": {"access_token": "X123"},
        },
        "api_keys": [],
    }

    for fake_api_key in fake_api_keys:
        api_key_entry = {
            "sha512_digest": fake_api_key["sha512_digest"],
            "salt": fake_api_key["salt"],
        }
        fake_config["api_keys"].append(api_key_entry)

    yield fake_config


@pytest.fixture
def fake_sync_lock():
    yield FakeThreadingLock()


@pytest.fixture
def fake_scheduler():
    yield FakeScheduler()


@pytest.fixture
def fake_artifactory_path():
    yield artifactory.ArtifactoryPath("http://127.0.0.1/artifactory/repo/")


@pytest.fixture
def fake_local_file(tmp_path):
    target_output = os.path.join(tmp_path, "helloworld.bin")
    with open(target_output, "w+") as target_output_f:
        target_output_f.write("abc12345")
    return target_output


@pytest.fixture
def fake_local_file_stat():
    yield FakeStat()
