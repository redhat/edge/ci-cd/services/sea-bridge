#!/usr/bin/env python3

"""Test main.py"""

import argparse
import pytest

from unittest.mock import patch

import sea_bridge.main


class FakeApp:
    """A fake flask app for testing"""

    def __init__(self):
        self.ran = 0

    def run(self, host=None, port=None):
        self.ran += 1


def test_argparser():
    """Test argparser()

    Checks argparser configuration results in the needed args.
    """
    argparser = sea_bridge.main.argparser()
    assert type(argparser) is argparse.ArgumentParser

    args = argparser.parse_args(["--config", "./test.yaml"])
    assert args
    assert str(args.config) == "test.yaml"


@pytest.mark.parametrize(
    "cmd,exception_type,exit_code",
    [
        ([], SystemExit, 2),
        (["--option"], SystemExit, 2),
        (["--config"], SystemExit, 2),
    ],
)
def test_argparser_invalid_args(cmd, exception_type, exit_code):
    """Test argparser() error cases

    Checks argparser throws appropriate errors and exits the parent script
    when invalid values are fed to it.
    """
    argparser = sea_bridge.main.argparser()
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        argparser.parse_args(cmd)

    assert pytest_wrapped_e.type == exception_type
    assert pytest_wrapped_e.value.code == exit_code


@patch("sys.argv", ["main", "--config", "test.yaml"])
@patch("sea_bridge.main.create_app")
@patch("sea_bridge.main.parse_and_validate")
def test_main(mock_parse_and_validate, mock_create_app):
    """Test main()

    Test that main() runs the expected subroutines (creating the flask app).
    """
    fake_app = FakeApp()

    mock_parse_and_validate.return_value = {
        "aws_s3": {},
        "artifactory": {},
        "interval": 100,
    }
    mock_create_app.return_value = fake_app

    sea_bridge.main.main()

    mock_create_app.assert_called_once()
    assert fake_app.ran == 1
