# sea-bridge

An *S3*-to-*A*rtifactory-bridge service. Aka s3a-bridge / sea-bridge.

## About

This project hosts a microservice that acts as a bridge between an S3 bucket
and an Artifactory repository. The purpose is to enable easy synchronous file
mirroring between the two repository places.

The microservice can be started with parameters indicating a timed cronjob to
synchronise the s3 bucket to an Artifactory repository or it can be triggered
via a REST api endpoint.

## Configuration

Sea-bridge takes as input via command line the path to a config.yml file. The
configuration is stored in the yaml file and must set the following options:

| Option                  | Description                                        |
| ---                     | ---                                                |
| s3_bucket               | Source S3 bucket to synchronise                    |
| s3_prefixes             | List of prefixes of source bucket to synchronise   |
| s3_credentials          | Credentials to access S3 bucket                    |
| artifactory_url         | URL of destination Artifactory instance            |
| artifactory_repo        | Destination Artifactory repo                       |
| artifactory_upload_prefix | Upload prefix for destination Artifactory repo   |
| artifactory_credentials | Credentials to access Artifactory repo             |
| api_keys                | API keys required to trigger /sync endpoint        |
| interval                | (Optional) Force synchronise on interval (minutes) |

A complete and properly formatted config.yml is mandatory in order to spawn a
service instance.

Template:

```yaml
name: sync-credential
aws_s3:
  s3_bucket: "source-bucket"
  s3_prefixes:
    - /prefix1
    - /prefix2
    - /prefix3
  s3_credentials:
    access_key_id: "not-secret-key-id"
    secret_access_key: "secret-key"
artifactory:
  artifactory_url: "https://artifactory.xyz/artifactory"
  artifactory_repo: "destination-repo"
  artifactory_upload_prefix: "upload-prefix"
  artifactory_credentials:
    access_token: "secret-token"
interval: 15
api_keys:
  - sha512_digest: xxx
    salt: xxx
```

If you are hosting sea-bridge in a Kubernetes based infrastructure, we suggest
that this yaml file be kept in a Secret and mounted to the Pod per the
Deployment configuration.
